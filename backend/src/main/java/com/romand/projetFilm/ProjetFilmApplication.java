package com.romand.projetFilm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetFilmApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetFilmApplication.class, args);
	}

}
