package com.romand.projetFilm.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ActeurDto {

    private String idActeur;

    @NotEmpty(message = "Veuillez renseigner le nom de l'acteur")
    @Size(min = 2, max = 80, message = "Le nom doit comporter au minimum 2 caractères")
    private String nom;

    @NotEmpty(message = "Veuillez renseigner le prenom de l'acteur")
    @Size(min = 2, max = 80, message = "Le prenom doit comporter au minimum 2 caractères")
    private String prenom;

    private Date dateNaissance;

    @NotEmpty(message = "Veuillez renseigner la nationalite de l'acteur")
    @Size(min = 5, max = 80, message = "La nationalite doit comporter au minimum 5 caractères")
    private String nationalite;

    private List<FilmDto> films;

    private List<SerieDto> series;
}
