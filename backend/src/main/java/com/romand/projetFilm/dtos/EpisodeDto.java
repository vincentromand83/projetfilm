package com.romand.projetFilm.dtos;

import com.romand.projetFilm.entities.Format;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EpisodeDto {

    private String idEpisode;

    private Format format;

    @NotEmpty(message = "Veuillez renseigner le nom de l'episode")
    @Size(min = 4, max = 80, message = "Le nom doit comporter au moins 4 caracteres")
    private String nom;

    @NotNull
    private SaisonDto saison;
}
