package com.romand.projetFilm.dtos;

import com.romand.projetFilm.entities.Format;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FilmDto {

    private String idFilm;

    @NotEmpty(message = "Veuillez renseigner le titre du film")
    @Size(max = 80)
    private String titre;

    @NotEmpty(message = "Veuillez renseigner le synopsis du film")
    private String synopsis;

    @NotEmpty(message = "Veuillez renseigner le format")
    private Format format;

    @NotBlank
    private Integer duree;

    private Date dateSortie;

    @Size(min = 5, max = 80, message = "Veuillez renseigner le pays")
    private String pays;

    @NotBlank(message = "Veuillez renseigner les acteurs du film")
    private List<ActeurDto> acteurs;

    @NotBlank(message = "Veuillez renseigner les realisateurs du film")
    private List<RealisateurDto> realisateurs;

    @NotBlank(message = "Veuillez renseigner les genres du film")
    private List<GenreDto> genres;
}
