package com.romand.projetFilm.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GenreDto {

    private String idGenre;

    @NotEmpty(message = "Veuillez renseigner le nom du genre")
    @Size(max = 50)
    private String nom;

    private List<FilmDto> films;

    private List<SerieDto> series;
}
