package com.romand.projetFilm.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RealisateurDto {

    private String idRealisateur;

    @NotEmpty(message = "Veuillez renseigner le nom du realisateur")
    @Size(min = 2, max = 80)
    private String nom;

    @NotEmpty(message = "Veuillez renseigner le prenom du realisateur")
    @Size(min = 2, max = 80)
    private String prenom;

    private Date dateNaissance;

    @NotEmpty(message = "Veuillez renseigner la nationalite du realisateur")
    @Size(min = 2, max = 80)
    private String nationalite;

    private List<FilmDto> films;

    private List<SerieDto> series;

}
