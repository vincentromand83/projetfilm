package com.romand.projetFilm.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoleDto {

    private String idRole;

    @NotEmpty(message = "Veuillez renseigner le nom du role")
    @Size(max = 15)
    private String roleName;

    private List<UtilisateurDto> utilisateurs;
}
