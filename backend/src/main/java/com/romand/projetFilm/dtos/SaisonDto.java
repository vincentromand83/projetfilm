package com.romand.projetFilm.dtos;

import com.romand.projetFilm.entities.Format;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SaisonDto {

    private String idSaison;

    @NotEmpty(message = "Veuillez renseigner le nom de la saison")
    @Size(min = 2, max = 20)
    private String nom;

    private Date dateSortie;

    private Format format;

    @NotBlank(message = "Veuillez renseigner a quelle serie appartient la saison")
    private SerieDto serie;

    private List<EpisodeDto> episodes;
}
