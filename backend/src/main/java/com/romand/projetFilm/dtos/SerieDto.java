package com.romand.projetFilm.dtos;

import com.romand.projetFilm.entities.Etat;
import com.romand.projetFilm.entities.Format;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SerieDto {

    private String idSerie;

    @NotEmpty(message = "Veuillez renseigner le synopsis du film")
    @Size(min = 30)
    private String synopsis;

    @NotEmpty(message = "Veuillez renseigner l'etat de la Serie")
    private Etat etat;

    @Size(max = 4)
    private Format format;

    private Date dateSortie;

    @NotBlank(message = "Veuillez renseigner les genres de la serie")
    private List<GenreDto> genres;

    @NotBlank(message = "Veuillez renseigner les acteurs de la Serie")
    private List<ActeurDto> acteurs;

    @NotBlank(message = "Veuillez renseigner les realisateurs de la Serie")
    private List<RealisateurDto> realisateurs;
}
