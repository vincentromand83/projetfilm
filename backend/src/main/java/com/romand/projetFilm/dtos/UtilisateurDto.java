package com.romand.projetFilm.dtos;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UtilisateurDto {

    private String idUtilisateur;

    @NotEmpty(message = "Veuillez renseigner le nom de l'utilisateur")
    @Size(min = 2, max = 80)
    private String nom;

    @NotEmpty(message = "Veuillez renseigner le prenom de l'utilisateur")
    @Size(min = 2, max = 80)
    private String prenom;

    @Email(message = "L'email doit être valide")
    private String email;

    @NotEmpty()
    @Size(min = 12, message = "Le mot de passe doit contenir au minimum 12 caractères")
    private String password;

    private List<RoleDto> roles;
}
