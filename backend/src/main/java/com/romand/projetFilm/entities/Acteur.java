package com.romand.projetFilm.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "acteur")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idActeur")
public class Acteur {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idActeur;

    @Column(nullable = false, length = 80)
    private String nom;

    @Column(nullable = false, length = 80)
    private String prenom;

    @Column(name = "date_naissance")
    private Date dateNaissance;

    @Column(nullable = false, length = 80)
    private String nationalite;

    @ManyToMany(mappedBy = "acteurs")
    @ToString.Exclude
    private List<Film> films;

    @ManyToMany(mappedBy = "acteurs")
    @ToString.Exclude
    private List<Serie> series;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Acteur acteur = (Acteur) o;
        return getIdActeur() != null && Objects.equals(getIdActeur(), acteur.getIdActeur());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
