package com.romand.projetFilm.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "episode")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idEpisode")
public class Episode {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idEpisode;

    @Column(nullable = false, length = 80)
    private String nom;

    @Enumerated(EnumType.STRING)
    private Format format;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idSaison")
    @ToString.Exclude
    private Saison saison;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Episode episode = (Episode) o;
        return getIdEpisode() != null && Objects.equals(getIdEpisode(), episode.getIdEpisode());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
