package com.romand.projetFilm.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "film")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idFilm")
public class Film {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idFilm;

    @Column(nullable = false, length = 100)
    private String titre;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String synopsis;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Format format;

    @Column(nullable = false)
    private Integer duree;

    @Column(name = "date_sortie")
    private Date dateSortie;

    @Column(length = 80)
    private String pays;

    @ManyToMany
    @JoinTable(name = "film_genre",
            joinColumns = @JoinColumn(name = "film_id", referencedColumnName = "idFilm"),
            inverseJoinColumns = @JoinColumn(name = "genre_id", referencedColumnName = "idGenre"))
    @ToString.Exclude
    private List<Genre> genres;

    @ManyToMany
    @JoinTable(name = "acteur_film",
            joinColumns = @JoinColumn(name = "film_id", referencedColumnName = "idFilm"),
            inverseJoinColumns = @JoinColumn(name = "acteur_id", referencedColumnName = "idActeur"))
    @ToString.Exclude
    private List<Acteur> acteurs;

    @ManyToMany
    @JoinTable(name = "realisateur_film",
            joinColumns = @JoinColumn(name = "film_id", referencedColumnName = "idFilm"),
            inverseJoinColumns = @JoinColumn(name = "realisateur_id", referencedColumnName = "idRealisateur"))
    @ToString.Exclude
    private List<Realisateur> realisateurs;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Film film = (Film) o;
        return getIdFilm() != null && Objects.equals(getIdFilm(), film.getIdFilm());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
