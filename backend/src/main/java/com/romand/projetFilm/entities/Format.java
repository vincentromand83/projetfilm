package com.romand.projetFilm.entities;

public enum Format {

    SD,
    H264,
    H265
}
