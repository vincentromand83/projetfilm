package com.romand.projetFilm.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "genre")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idGenre")
public class Genre {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idGenre;

    @Column(nullable = false, length = 50)
    private String nom;

    @ManyToMany(mappedBy = "genres")
    @ToString.Exclude
    private List<Film> films;

    @ManyToMany(mappedBy = "genres")
    @ToString.Exclude
    private List<Serie> series;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Genre genre = (Genre) o;
        return getIdGenre() != null && Objects.equals(getIdGenre(), genre.getIdGenre());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
