package com.romand.projetFilm.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "saison")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idSaison")
public class Saison {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idSaison;

    @Column(nullable = false, length = 20)
    private String nom;

    @Enumerated(EnumType.STRING)
    private Format format;

    @Column(name = "date_sortie")
    private Date dateSortie;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "idSerie")
    @ToString.Exclude
    private Serie serie;

    @OneToMany(mappedBy = "saison")
    @ToString.Exclude
    private List<Episode> episodes;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Saison saison = (Saison) o;
        return getIdSaison() != null && Objects.equals(getIdSaison(), saison.getIdSaison());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
