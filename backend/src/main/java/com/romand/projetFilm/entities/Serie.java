package com.romand.projetFilm.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "serie")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idSerie")
public class Serie {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String idSerie;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String synopsis;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Etat etat;

    @Enumerated(EnumType.STRING)
    private Format format;

    @Column(name = "date_sortie")
    private Date dateSortie;

    @ManyToMany
    @JoinTable(name = "serie_genre",
            joinColumns = @JoinColumn(name = "serie_id", referencedColumnName = "idSerie"),
            inverseJoinColumns = @JoinColumn(name = "genre_id", referencedColumnName = "idGenre"))
    @ToString.Exclude
    private List<Genre> genres;

    @ManyToMany
    @JoinTable(name = "acteur_serie",
            joinColumns = @JoinColumn(name = "serie_id", referencedColumnName = "idSerie"),
            inverseJoinColumns = @JoinColumn(name = "acteur_id", referencedColumnName = "idActeur"))
    @ToString.Exclude
    private List<Acteur> acteurs;

    @ManyToMany
    @JoinTable(name = "realisateur_serie",
            joinColumns = @JoinColumn(name = "serie_id", referencedColumnName = "idSerie"),
            inverseJoinColumns = @JoinColumn(name = "realisateur_id", referencedColumnName = "idRealisateur"))
    @ToString.Exclude
    private List<Realisateur> realisateurs;

    @OneToMany(mappedBy = "serie")
    @ToString.Exclude
    private List<Saison> saisons;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Serie serie = (Serie) o;
        return getIdSerie() != null && Objects.equals(getIdSerie(), serie.getIdSerie());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
