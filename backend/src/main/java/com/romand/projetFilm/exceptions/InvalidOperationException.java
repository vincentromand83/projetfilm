package com.romand.projetFilm.exceptions;

public class InvalidOperationException extends RuntimeException{

    public InvalidOperationException(String message) { super(message);}
}
