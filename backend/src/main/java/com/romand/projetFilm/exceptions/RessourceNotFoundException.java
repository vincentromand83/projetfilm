package com.romand.projetFilm.exceptions;

public class RessourceNotFoundException extends RuntimeException{

    public RessourceNotFoundException(String message) { super(message);}
}
