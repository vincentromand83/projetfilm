package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.ActeurDto;
import com.romand.projetFilm.entities.Acteur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ActeurMapper {

    Acteur acteurDtoToActeur(ActeurDto acteurDto);

    ActeurDto acteurToActeurDto(Acteur acteur);
}
