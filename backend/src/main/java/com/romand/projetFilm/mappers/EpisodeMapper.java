package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.EpisodeDto;
import com.romand.projetFilm.entities.Episode;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EpisodeMapper {

    Episode episodeDtoToEpisode(EpisodeDto episodeDto);

    EpisodeDto episodeToEpisodeDto(Episode episode);
}
