package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.FilmDto;
import com.romand.projetFilm.entities.Film;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FilmMapper {

    Film filmDtoToFilm(FilmDto filmDto);

    FilmDto filmToFilmDto(Film film);
}
