package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.GenreDto;
import com.romand.projetFilm.entities.Genre;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface GenreMapper {

    Genre genreDtoToGenre(GenreDto genreDto);

    GenreDto genreToGenreDto(Genre genre);
}
