package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.RealisateurDto;
import com.romand.projetFilm.entities.Realisateur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RealisateurMapper {

    Realisateur realisateurDtoToRealisateur(RealisateurDto realisateurDto);

    RealisateurDto realisateurToRealisateurDto(Realisateur realisateur);
}
