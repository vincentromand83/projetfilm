package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.RoleDto;
import com.romand.projetFilm.entities.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    Role roleDtoToRole(RoleDto roleDto);

    RoleDto roleToRoleDto(Role role);
}
