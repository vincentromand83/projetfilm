package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.SaisonDto;
import com.romand.projetFilm.entities.Saison;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SaisonMapper {

    Saison saisonDtoToSaison(SaisonDto saisonDto);

    SaisonDto saisonToSaisonDto(Saison saison);
}
