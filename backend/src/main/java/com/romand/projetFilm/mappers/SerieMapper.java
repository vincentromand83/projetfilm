package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.SerieDto;
import com.romand.projetFilm.entities.Serie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SerieMapper {

    Serie serieDtoToSerie(SerieDto serieDto);

    SerieDto serieToSerieDto(Serie serie);
}
