package com.romand.projetFilm.mappers;

import com.romand.projetFilm.dtos.UtilisateurDto;
import com.romand.projetFilm.entities.Utilisateur;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UtilisateurMapper {

    Utilisateur utilisateurDtoToUtilisateur(UtilisateurDto utilisateurDto);

    UtilisateurDto utilisateurToUtilisateurDto(Utilisateur utilisateur);
}
