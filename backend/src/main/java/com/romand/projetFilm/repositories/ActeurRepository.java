package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Acteur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActeurRepository extends JpaRepository<Acteur, String> {

    Page<Acteur> findByFilmsIdFilm(String idFilm, Pageable pageable);

    Page<Acteur> findBySeriesIdSerie(String idSerie, Pageable pageable);
}
