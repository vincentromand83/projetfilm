package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Episode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EpisodeRepository extends JpaRepository<Episode, String> {

    Page<Episode> findBySaisonIdSaison(String idSaison, Pageable pageable);
}
