package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilmRepository extends JpaRepository<Film, String> {

    Page<Film> findByActeursIdActeur(String idActeur, Pageable pageable);

    List<Film> findByActeursIdActeur(String idActeur);

    Page<Film> findByGenresIdGenre(String idGenre, Pageable pageable);

    List<Film> findByGenresIdGenre(String idGGenre);

    Page<Film> findByRealisateursIdRealisateur(String idRealisateur, Pageable pageable);

    List<Film> findByRealisateursIdRealisateur(String idRealisateur);
}
