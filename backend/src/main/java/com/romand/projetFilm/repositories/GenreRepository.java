package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Genre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, String> {

    Page<Genre> findByFilmsIdFilm(String idFilm, Pageable pageable);

    Page<Genre> findBySeriesIdSerie(String idSerie, Pageable pageable);
}
