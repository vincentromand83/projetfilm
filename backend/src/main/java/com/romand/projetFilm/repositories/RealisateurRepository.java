package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Realisateur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RealisateurRepository extends JpaRepository<Realisateur, String> {

    Page<Realisateur> findByFilmsIdFilm(String idFilm, Pageable pageable);

    Page<Realisateur> findBySeriesIdSerie(String idSerie, Pageable pageable);
}
