package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, String> {

    Page<Role> findByUtilisateursIdUtilisateur(String idUtilisateur, Pageable pageable);
}
