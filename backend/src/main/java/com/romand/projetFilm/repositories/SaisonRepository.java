package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Saison;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaisonRepository extends JpaRepository<Saison, String> {

    Page<Saison> findBySerieIdSerie(String idSerie, Pageable pageable);
}
