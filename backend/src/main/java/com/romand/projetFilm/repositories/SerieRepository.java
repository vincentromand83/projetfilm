package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Serie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SerieRepository extends JpaRepository<Serie, String> {

    Page<Serie> findByActeursIdActeur(String idActeur, Pageable pageable);

    List<Serie> findByActeursIdActeur(String idActeur);

    Page<Serie> findByGenresIdGenre(String idGenre, Pageable pageable);

    List<Serie> findByGenresIdGenre(String idGenre);

    Page<Serie> findByRealisateursIdRealisateur(String idRealisateur, Pageable pageable);

    List<Serie> findByRealisateursIdRealisateur(String idRealisateur);

    Page<Serie> findBySaisonsIdSaison(String idSaison, Pageable pageable);

    List<Serie> findBySaisonsIdSaison(String idSaison);
}
