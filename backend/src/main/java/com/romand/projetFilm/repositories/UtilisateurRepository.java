package com.romand.projetFilm.repositories;

import com.romand.projetFilm.entities.Utilisateur;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, String > {

    Page<Utilisateur> findByRolesIdRole(String idRole, Pageable pageable);

    List<Utilisateur> findByRolesIdRole(String idRole);
}
