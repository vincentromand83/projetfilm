package com.romand.projetFilm.services;

import com.romand.projetFilm.dtos.ActeurDto;
import org.springframework.data.domain.Page;

public interface ActeurService {

    ActeurDto getActeurById(String idActeur);

    Page<ActeurDto> getAllActeurs(int page, int size);

    Page<ActeurDto> getAllActeursByIdFilm(String idFilm, int page, int size);

    Page<ActeurDto> getAllActeursByIdSerie(String idSerie, int page, int size);

    ActeurDto createActeur(ActeurDto acteurDto);

    ActeurDto updateActeur(String idActeur, ActeurDto acteurDto);

    String deleteActeurById(String idActeur);
}
