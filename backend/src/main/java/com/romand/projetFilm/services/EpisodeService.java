package com.romand.projetFilm.services;

import com.romand.projetFilm.dtos.EpisodeDto;
import org.springframework.data.domain.Page;

public interface EpisodeService {

    EpisodeDto getEpisodeById(String idEpisode);

    Page<EpisodeDto> getAllEpisodes(int page, int size);

    Page<EpisodeDto> getAllEpisodesByIdSaison(String idSaison, int page, int size);

    EpisodeDto createEpisode(EpisodeDto episodeDto);

    EpisodeDto updateEpisode(String idEpisode, EpisodeDto episodeDto);

    String deleteEpisodeById(String idEpisode);
}
