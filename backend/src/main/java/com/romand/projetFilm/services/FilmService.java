package com.romand.projetFilm.services;

import com.romand.projetFilm.dtos.FilmDto;
import org.springframework.data.domain.Page;

public interface FilmService {

    FilmDto getFilmById(String idFilm);

    Page<FilmDto> getAllFilms(int page, int size);

    Page<FilmDto> getAllFilmsByIdActeur(String idActeur, int page, int size);

    Page<FilmDto> getAllFilmsByIdGenre(String idGenre, int page, int size);

    Page<FilmDto> getAllFilmsByIdRealisateur(String idRealisateur, int page, int size);

    FilmDto createFilm(FilmDto filmDto);

    FilmDto updateFilm(String idFilm, FilmDto filmDto);

    String deleteFilmById(String idFilm);
}
