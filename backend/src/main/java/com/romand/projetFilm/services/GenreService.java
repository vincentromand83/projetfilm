package com.romand.projetFilm.services;

import com.romand.projetFilm.dtos.GenreDto;
import org.springframework.data.domain.Page;

public interface GenreService {

    GenreDto getGenreById(String idGenre);

    Page<GenreDto> getAllGenres(int page, int size);

    Page<GenreDto> getAllGenresByIdFilm(String idFilm, int page, int size);

    Page<GenreDto> getAllGenresByIdSerie(String idSerie, int page, int size);

    GenreDto createGenre(GenreDto genreDto);

    GenreDto updateGenre(String idGenre, GenreDto genreDto);

    String deleteGenreById(String idGenre);
}
