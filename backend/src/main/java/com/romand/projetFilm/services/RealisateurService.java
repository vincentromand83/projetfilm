package com.romand.projetFilm.services;

import com.romand.projetFilm.dtos.RealisateurDto;
import org.springframework.data.domain.Page;

public interface RealisateurService {

    RealisateurDto getRealisateurById(String idRealisateur);

    Page<RealisateurDto> getAllRealisateurs(int page, int size);

    Page<RealisateurDto> getAllRealisateursByIdFilm(String idFilm, int page, int size);

    Page<RealisateurDto> getAllRealisateursByIdSerie(String idSerie, int page, int size);

    RealisateurDto createRealisateur(RealisateurDto realisateurDto);

    RealisateurDto updateRealisateur(String idRealisateur, RealisateurDto realisateurDto);

    String deleteRealisateurById(String idRealisateur);
}
