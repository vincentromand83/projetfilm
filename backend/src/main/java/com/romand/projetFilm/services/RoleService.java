package com.romand.projetFilm.services;

import com.romand.projetFilm.dtos.RoleDto;
import org.springframework.data.domain.Page;

public interface RoleService {

    RoleDto getRoleById(String idRole);

    Page<RoleDto> getAllRoles(int page, int size);

    Page<RoleDto> getAllRolesByIdUtilisateur(String idUtilisateur, int page, int size);

    RoleDto createRole(RoleDto roleDto);

    RoleDto updateRole(String idRole, RoleDto roleDto);

    String deleteRoleById(String idRole);
}
