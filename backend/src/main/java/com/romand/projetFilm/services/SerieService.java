package com.romand.projetFilm.services;

import com.romand.projetFilm.dtos.SerieDto;
import org.springframework.data.domain.Page;

public interface SerieService {

    SerieDto getSerieById(String idSerie);

    Page<SerieDto> getAllSeries(int page, int size);

    Page<SerieDto> getAllSeriesByIdActeur(String idActeur, int page, int size);

    Page<SerieDto> getAllSeriesByIdGenre(String idGenre, int page, int size);

    Page<SerieDto> getAllSeriesByIdRealisateur(String idRealisateur, int page, int size);

    SerieDto createSerie(SerieDto serieDto);

    SerieDto updateSerie(String idSerie, SerieDto serieDto);

    String deleteSerieById(String idSerie);
}
