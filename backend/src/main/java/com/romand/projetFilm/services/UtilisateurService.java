package com.romand.projetFilm.services;

import com.romand.projetFilm.dtos.UtilisateurDto;
import org.springframework.data.domain.Page;

public interface UtilisateurService {

    UtilisateurDto getUtilisateurById(String idUtilisateur);

    Page<UtilisateurDto> getAllUTilisateurs(int page, int size);

    Page<UtilisateurDto> getAllUtilisateursByIdRole(String idUTilisateur, int page, int size);

    UtilisateurDto createUtilisateur(UtilisateurDto utilisateurDto);

    UtilisateurDto updateUtilisateur(String idUtilisateur, UtilisateurDto utilisateurDto);

    String deleteUtilisateurById(String idUtilisateur);
}
