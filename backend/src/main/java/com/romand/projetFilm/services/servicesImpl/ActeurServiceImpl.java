package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.ActeurDto;
import com.romand.projetFilm.entities.Film;
import com.romand.projetFilm.entities.Serie;
import com.romand.projetFilm.exceptions.InvalidOperationException;
import com.romand.projetFilm.mappers.ActeurMapper;
import com.romand.projetFilm.repositories.ActeurRepository;
import com.romand.projetFilm.repositories.FilmRepository;
import com.romand.projetFilm.repositories.SerieRepository;
import com.romand.projetFilm.services.ActeurService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ActeurServiceImpl implements ActeurService {
    private final ActeurRepository acteurRepository;
    private final FilmRepository filmRepository;
    private final SerieRepository serieRepository;
    private final ActeurMapper acteurMapper;

    public ActeurServiceImpl(ActeurRepository acteurRepository, FilmRepository filmRepository,
                             SerieRepository serieRepository, ActeurMapper acteurMapper) {
        this.acteurRepository = acteurRepository;
        this.filmRepository = filmRepository;
        this.serieRepository = serieRepository;
        this.acteurMapper = acteurMapper;
    }

    @Override
    public ActeurDto getActeurById(String idActeur) {
        return acteurRepository.findById(idActeur).map(acteurMapper::acteurToActeurDto).orElseThrow(()->
                new EntityNotFoundException(
                        "Aucun acteur avec l'Id " + idActeur + "n'est present dans la BDD"));
    }
    @Override
    public Page<ActeurDto> getAllActeurs(int page, int size) {
        return acteurRepository.findAll(PageRequest.of(page, size)).map(acteurMapper::acteurToActeurDto);
    }
    @Override
    public Page<ActeurDto> getAllActeursByIdFilm(String idFilm, int page, int size) {
        return acteurRepository.findByFilmsIdFilm(idFilm, PageRequest.of(page, size))
                .map(acteurMapper::acteurToActeurDto);
    }
    @Override
    public Page<ActeurDto> getAllActeursByIdSerie(String idSerie, int page, int size) {
        return acteurRepository.findBySeriesIdSerie(idSerie, PageRequest.of(page, size))
                .map(acteurMapper::acteurToActeurDto);
    }
    @Override
    public ActeurDto createActeur(ActeurDto acteurDto) {
        return acteurMapper.acteurToActeurDto(acteurRepository.save(acteurMapper.acteurDtoToActeur(acteurDto)));
    }
    @Override
    public ActeurDto updateActeur(String idActeur, ActeurDto acteurDto) {
        var acteur = acteurMapper.acteurDtoToActeur(getActeurById(idActeur));
        acteur = acteurMapper.acteurDtoToActeur(acteurDto);
        acteur.setIdActeur(idActeur);
        return createActeur(acteurMapper.acteurToActeurDto(acteur));
    }
    @Override
    public String deleteActeurById(String idActeur) {
        List<Film> films = filmRepository.findByActeursIdActeur(idActeur);
        if(!films.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer un acteur présent dans un film");
        }
        List<Serie> series = serieRepository.findByActeursIdActeur(idActeur);
        if(!series.isEmpty()) {
            throw new InvalidOperationException("Impossible de surpprimer un acteur présent dans une série");
        }
        acteurRepository.delete(acteurMapper.acteurDtoToActeur(getActeurById(idActeur)));
        return "L'acteur a été supprimé avec suucés";
    }
}
