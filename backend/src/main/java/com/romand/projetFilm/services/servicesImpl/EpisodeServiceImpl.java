package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.EpisodeDto;
import com.romand.projetFilm.mappers.EpisodeMapper;
import com.romand.projetFilm.repositories.EpisodeRepository;
import com.romand.projetFilm.services.EpisodeService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class EpisodeServiceImpl implements EpisodeService {
    private final EpisodeRepository episodeRepository;
    private final EpisodeMapper episodeMapper;

    public EpisodeServiceImpl(EpisodeRepository episodeRepository, EpisodeMapper episodeMapper) {
        this.episodeRepository = episodeRepository;
        this.episodeMapper = episodeMapper;
    }

    @Override
    public EpisodeDto getEpisodeById(String idEpisode) {
        return episodeRepository.findById(idEpisode).map(episodeMapper::episodeToEpisodeDto).orElseThrow(()->
                new EntityNotFoundException("Impossible de trouver l'épisode avec l'id" + idEpisode +
                        "dans la BDD"));
    }
    @Override
    public Page<EpisodeDto> getAllEpisodes(int page, int size) {
        return episodeRepository.findAll(PageRequest.of(page, size)).map(episodeMapper::episodeToEpisodeDto);
    }
    @Override
    public Page<EpisodeDto> getAllEpisodesByIdSaison(String idSaison, int page, int size) {
        return episodeRepository.findBySaisonIdSaison(idSaison, PageRequest.of(page, size))
                .map(episodeMapper::episodeToEpisodeDto);
    }
    @Override
    public EpisodeDto createEpisode(EpisodeDto episodeDto) {
        return episodeMapper.episodeToEpisodeDto(episodeRepository.save(episodeMapper.episodeDtoToEpisode(episodeDto)));
    }
    @Override
    public EpisodeDto updateEpisode(String idEpisode, EpisodeDto episodeDto) {
        var episode = episodeMapper.episodeDtoToEpisode(getEpisodeById(idEpisode));
        episode = episodeMapper.episodeDtoToEpisode(episodeDto);
        episode.setIdEpisode(idEpisode);
        return createEpisode(episodeMapper.episodeToEpisodeDto(episode));
    }
    @Override
    public String deleteEpisodeById(String idEpisode) {
        episodeRepository.delete(episodeMapper.episodeDtoToEpisode(getEpisodeById(idEpisode)));
        return "L'Episode a été supprimé avec succes";
    }
}
