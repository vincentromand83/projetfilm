package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.FilmDto;
import com.romand.projetFilm.mappers.FilmMapper;
import com.romand.projetFilm.repositories.FilmRepository;
import com.romand.projetFilm.services.FilmService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class FilmServiceImpl implements FilmService {

    private final FilmRepository filmRepository;
    private final FilmMapper filmMapper;

    public FilmServiceImpl(FilmRepository filmRepository, FilmMapper filmMapper) {
        this.filmRepository = filmRepository;
        this.filmMapper = filmMapper;
    }
    @Override
    public FilmDto getFilmById(String idFilm) {
        return filmRepository.findById(idFilm).map(filmMapper::filmToFilmDto).orElseThrow(()->
                new EntityNotFoundException("Impossible de trouver le film avec l'id " + idFilm + "dans la BDD"));
    }
    @Override
    public Page<FilmDto> getAllFilms(int page, int size) {
        return filmRepository.findAll(PageRequest.of(page, size)).map(filmMapper::filmToFilmDto);
    }
    @Override
    public Page<FilmDto> getAllFilmsByIdActeur(String idActeur, int page, int size) {
        return filmRepository.findByActeursIdActeur(idActeur, PageRequest.of(page, size)).map(filmMapper::filmToFilmDto);
    }
    @Override
    public Page<FilmDto> getAllFilmsByIdGenre(String idGenre, int page, int size) {
        return filmRepository.findByGenresIdGenre(idGenre, PageRequest.of(page, size)).map(filmMapper::filmToFilmDto);
    }
    @Override
    public Page<FilmDto> getAllFilmsByIdRealisateur(String idRealisateur, int page, int size) {
        return filmRepository.findByRealisateursIdRealisateur(idRealisateur, PageRequest.of(page, size))
                .map(filmMapper::filmToFilmDto);
    }
    @Override
    public FilmDto createFilm(FilmDto filmDto) {
        return filmMapper.filmToFilmDto(filmRepository.save(filmMapper.filmDtoToFilm(filmDto)));
    }
    @Override
    public FilmDto updateFilm(String idFilm, FilmDto filmDto) {
        var film = filmMapper.filmDtoToFilm(getFilmById(idFilm));
        film = filmMapper.filmDtoToFilm(filmDto);
        film.setIdFilm(idFilm);
        return createFilm(filmMapper.filmToFilmDto(film));
    }
    @Override
    public String deleteFilmById(String idFilm) {
        filmRepository.delete(filmMapper.filmDtoToFilm(getFilmById(idFilm)));
        return "Le Film a ete supprime avec succes";
    }
}