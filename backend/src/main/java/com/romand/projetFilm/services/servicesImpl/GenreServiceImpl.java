package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.GenreDto;
import com.romand.projetFilm.entities.Film;
import com.romand.projetFilm.entities.Serie;
import com.romand.projetFilm.exceptions.InvalidOperationException;
import com.romand.projetFilm.mappers.GenreMapper;
import com.romand.projetFilm.repositories.FilmRepository;
import com.romand.projetFilm.repositories.GenreRepository;
import com.romand.projetFilm.repositories.SerieRepository;
import com.romand.projetFilm.services.GenreService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;
    private final FilmRepository filmRepository;
    private final SerieRepository serieRepository;
    private final GenreMapper genreMapper;

    public GenreServiceImpl(GenreRepository genreRepository, FilmRepository filmRepository,
                            SerieRepository serieRepository, GenreMapper genreMapper) {
        this.genreRepository = genreRepository;
        this.filmRepository = filmRepository;
        this.serieRepository = serieRepository;
        this.genreMapper = genreMapper;
    }

    @Override
    public GenreDto getGenreById(String idGenre) {
        return genreRepository.findById(idGenre).map(genreMapper::genreToGenreDto).orElseThrow(()->
                new EntityNotFoundException("Impossible de trouver le genre avec l'id " + idGenre + "dans la BDD"));
    }
    @Override
    public Page<GenreDto> getAllGenres(int page, int size) {
        return genreRepository.findAll(PageRequest.of(page, size)).map(genreMapper::genreToGenreDto);
    }
    @Override
    public Page<GenreDto> getAllGenresByIdFilm(String idFilm, int page, int size) {
        return genreRepository.findByFilmsIdFilm(idFilm, PageRequest.of(page, size)).map(genreMapper::genreToGenreDto);
    }
    @Override
    public Page<GenreDto> getAllGenresByIdSerie(String idSerie, int page, int size) {
        return genreRepository.findBySeriesIdSerie(idSerie, PageRequest.of(page, size)).map(genreMapper::genreToGenreDto);
    }
    @Override
    public GenreDto createGenre(GenreDto genreDto) {
        return genreMapper.genreToGenreDto(genreRepository.save(genreMapper.genreDtoToGenre(genreDto)));
    }
    @Override
    public GenreDto updateGenre(String idGenre, GenreDto genreDto) {
        var genre = genreMapper.genreDtoToGenre(getGenreById(idGenre));
        genre = genreMapper.genreDtoToGenre(genreDto);
        genre.setIdGenre(idGenre);
        return createGenre(genreMapper.genreToGenreDto(genre));
    }
    @Override
    public String deleteGenreById(String idGenre) {
        List<Film> films = filmRepository.findByGenresIdGenre(idGenre);
        if(!films.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer un genre utilisé dans un film");
        }
        List<Serie> series = serieRepository.findByGenresIdGenre(idGenre);
        if(!series.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer un genre utilisé dans une serie");
        }
        genreRepository.delete(genreMapper.genreDtoToGenre(getGenreById(idGenre)));
        return "Le genre a été supprimé avec succés";
    }
}
