package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.RealisateurDto;
import com.romand.projetFilm.entities.Film;
import com.romand.projetFilm.entities.Serie;
import com.romand.projetFilm.exceptions.InvalidOperationException;
import com.romand.projetFilm.mappers.RealisateurMapper;
import com.romand.projetFilm.repositories.FilmRepository;
import com.romand.projetFilm.repositories.GenreRepository;
import com.romand.projetFilm.repositories.RealisateurRepository;
import com.romand.projetFilm.repositories.SerieRepository;
import com.romand.projetFilm.services.RealisateurService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RealisateurServiceImpl implements RealisateurService {

    private final RealisateurRepository realisateurRepository;
    private final FilmRepository filmRepository;
    private final SerieRepository serieRepository;
    private final RealisateurMapper realisateurMapper;
    private final GenreRepository genreRepository;

    public RealisateurServiceImpl(RealisateurRepository realisateurRepository, FilmRepository filmRepository,
                                  SerieRepository serieRepository, RealisateurMapper realisateurMapper,
                                  GenreRepository genreRepository) {
        this.realisateurRepository = realisateurRepository;
        this.filmRepository = filmRepository;
        this.serieRepository = serieRepository;
        this.realisateurMapper = realisateurMapper;
        this.genreRepository = genreRepository;
    }

    @Override
    public RealisateurDto getRealisateurById(String idRealisateur) {
        return realisateurRepository.findById(idRealisateur).map(realisateurMapper::realisateurToRealisateurDto)
                .orElseThrow(()-> new EntityNotFoundException(
                        "Aucun realisateur avec l'ID "+idRealisateur+ "n'a ete trouvé dans la BDD"));
    }
    @Override
    public Page<RealisateurDto> getAllRealisateurs(int page, int size) {
        return realisateurRepository.findAll(PageRequest.of(page, size))
                .map(realisateurMapper::realisateurToRealisateurDto);
    }
    @Override
    public Page<RealisateurDto> getAllRealisateursByIdFilm(String idFilm, int page, int size) {
        return realisateurRepository.findByFilmsIdFilm(idFilm, PageRequest.of(page, size))
                .map(realisateurMapper::realisateurToRealisateurDto);
    }
    @Override
    public Page<RealisateurDto> getAllRealisateursByIdSerie(String idSerie, int page, int size) {
        return realisateurRepository.findBySeriesIdSerie(idSerie, PageRequest.of(page, size))
                .map(realisateurMapper::realisateurToRealisateurDto);
    }
    @Override
    public RealisateurDto createRealisateur(RealisateurDto realisateurDto) {
        return realisateurMapper.realisateurToRealisateurDto(
                realisateurRepository.save(realisateurMapper.realisateurDtoToRealisateur(realisateurDto)));
    }
    @Override
    public RealisateurDto updateRealisateur(String idRealisateur, RealisateurDto realisateurDto) {
        var realisateur = realisateurMapper.realisateurDtoToRealisateur(getRealisateurById(idRealisateur));
        realisateur = realisateurMapper.realisateurDtoToRealisateur(realisateurDto);
        realisateur.setIdRealisateur(idRealisateur);
        return createRealisateur(realisateurMapper.realisateurToRealisateurDto(realisateur));
    }
    @Override
    public String deleteRealisateurById(String idRealisateur) {
        List<Film> films = filmRepository.findByRealisateursIdRealisateur(idRealisateur);
        if(!films.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer un réalisateur présent dans un film");
        }
        List<Serie> series = serieRepository.findByRealisateursIdRealisateur(idRealisateur);
        if(!series.isEmpty()) {
            throw new InvalidOperationException("Imposseible de supprimer un réalisateur present dans une serie");
        }
        realisateurRepository.delete(realisateurMapper.realisateurDtoToRealisateur(getRealisateurById(idRealisateur)));
        return "Le realisateur a ete supprime avec succes";
    }
}
