package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.RoleDto;
import com.romand.projetFilm.entities.Utilisateur;
import com.romand.projetFilm.exceptions.InvalidOperationException;
import com.romand.projetFilm.mappers.RoleMapper;
import com.romand.projetFilm.repositories.RoleRepository;
import com.romand.projetFilm.repositories.UtilisateurRepository;
import com.romand.projetFilm.services.RoleService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final UtilisateurRepository utilisateurRepository;
    private final RoleMapper roleMapper;

    public RoleServiceImpl(RoleRepository roleRepository, UtilisateurRepository utilisateurRepository,
                           RoleMapper roleMapper) {
        this.roleRepository = roleRepository;
        this.utilisateurRepository = utilisateurRepository;
        this.roleMapper = roleMapper;
    }

    @Override
    public RoleDto getRoleById(String idRole) {
        return roleRepository.findById(idRole).map(roleMapper::roleToRoleDto).orElseThrow(()->
                new EntityNotFoundException("Aucun role avec l'ID "+idRole+"n'a ete trouve dans la BDD"));
    }
    @Override
    public Page<RoleDto> getAllRoles(int page, int size) {
        return roleRepository.findAll(PageRequest.of(page, size)).map(roleMapper::roleToRoleDto);
    }
    @Override
    public Page<RoleDto> getAllRolesByIdUtilisateur(String idUtilisateur, int page, int size) {
        return roleRepository.findByUtilisateursIdUtilisateur(idUtilisateur, PageRequest.of(page, size))
                .map(roleMapper::roleToRoleDto);
    }
    @Override
    public RoleDto createRole(RoleDto roleDto) {
        return roleMapper.roleToRoleDto(roleRepository.save(roleMapper.roleDtoToRole(roleDto)));
    }
    @Override
    public RoleDto updateRole(String idRole, RoleDto roleDto) {
        var role = roleMapper.roleDtoToRole(getRoleById(idRole));
        role = roleMapper.roleDtoToRole(roleDto);
        role.setIdRole(idRole);
        return createRole(roleMapper.roleToRoleDto(role));
    }
    @Override
    public String deleteRoleById(String idRole) {
        List<Utilisateur> utilisateurs = utilisateurRepository.findByRolesIdRole(idRole);
        if(!utilisateurs.isEmpty()){
            throw new InvalidOperationException("Impossible de supprimer un role utilisé par un utilisateur");
        }
        roleRepository.delete(roleMapper.roleDtoToRole(getRoleById(idRole)));
        return "Le role a ete supprime avec succes";
    }
}
