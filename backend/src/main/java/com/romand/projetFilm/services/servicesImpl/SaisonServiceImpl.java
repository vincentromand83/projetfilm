package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.SaisonDto;
import com.romand.projetFilm.entities.Serie;
import com.romand.projetFilm.exceptions.InvalidOperationException;
import com.romand.projetFilm.mappers.SaisonMapper;
import com.romand.projetFilm.repositories.SaisonRepository;
import com.romand.projetFilm.repositories.SerieRepository;
import com.romand.projetFilm.services.SaisonService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaisonServiceImpl implements SaisonService {

    private final SaisonRepository saisonRepository;
    private final SerieRepository serieRepository;
    private final SaisonMapper saisonMapper;

    public SaisonServiceImpl(SaisonRepository saisonRepository, SerieRepository serieRepository,
                             SaisonMapper saisonMapper) {
        this.saisonRepository = saisonRepository;
        this.serieRepository = serieRepository;
        this.saisonMapper = saisonMapper;
    }

    @Override
    public SaisonDto getSaisonById(String idSaison) {
        return saisonRepository.findById(idSaison).map(saisonMapper::saisonToSaisonDto).orElseThrow(()->
                new EntityNotFoundException("Aucune saison avec l'ID "+idSaison+"n'a ete trouve dans la BDD"));
    }
    @Override
    public Page<SaisonDto> getAllSaisons(int page, int size) {
        return saisonRepository.findAll(PageRequest.of(page, size)).map(saisonMapper::saisonToSaisonDto);
    }
    @Override
    public Page<SaisonDto> getAllSaisonsByIdSerie(String idSerie, int page, int size) {
        return saisonRepository.findBySerieIdSerie(idSerie, PageRequest.of(page, size))
                .map(saisonMapper::saisonToSaisonDto);
    }
    @Override
    public SaisonDto createSaison(SaisonDto saisonDto) {
        return saisonMapper.saisonToSaisonDto(saisonRepository.save(saisonMapper.saisonDtoToSaison(saisonDto)));
    }
    @Override
    public SaisonDto updateSaison(String idSaison, SaisonDto saisonDto) {
        var saison = saisonMapper.saisonDtoToSaison(getSaisonById(idSaison));
        saison = saisonMapper.saisonDtoToSaison(saisonDto);
        saison.setIdSaison(idSaison);
        return createSaison(saisonMapper.saisonToSaisonDto(saison));
    }
    @Override
    public String deleteSaisonById(String idSaison) {
        List<Serie> series = serieRepository.findBySaisonsIdSaison(idSaison);
        if(!series.isEmpty()) {
            throw new InvalidOperationException("Impossible de supprimer une saison présente dans une serie");
        }
        saisonRepository.delete(saisonMapper.saisonDtoToSaison(getSaisonById(idSaison)));
        return "La saison a ete supprime avec succes" ;
    }
}
