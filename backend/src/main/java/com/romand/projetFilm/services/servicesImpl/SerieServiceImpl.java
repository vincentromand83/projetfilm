package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.SerieDto;
import com.romand.projetFilm.mappers.SerieMapper;
import com.romand.projetFilm.repositories.SerieRepository;
import com.romand.projetFilm.services.SerieService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class SerieServiceImpl implements SerieService {

    private final SerieRepository serieRepository;
    private final SerieMapper serieMapper;

    public SerieServiceImpl(SerieRepository serieRepository, SerieMapper serieMapper) {
        this.serieRepository = serieRepository;
        this.serieMapper = serieMapper;
    }

    @Override
    public SerieDto getSerieById(String idSerie) {
        return serieRepository.findById(idSerie).map(serieMapper::serieToSerieDto).orElseThrow(()->
                new EntityNotFoundException("Aucune Serie avec l''ID "+idSerie+ "n'a ete trouve dans la BDD"));
    }
    @Override
    public Page<SerieDto> getAllSeries(int page, int size) {
        return serieRepository.findAll(PageRequest.of(page, size)).map(serieMapper::serieToSerieDto);
    }
    @Override
    public Page<SerieDto> getAllSeriesByIdActeur(String idActeur, int page, int size) {
        return serieRepository.findByActeursIdActeur(idActeur,PageRequest.of(page, size)).map(serieMapper::serieToSerieDto);
    }
    @Override
    public Page<SerieDto> getAllSeriesByIdGenre(String idGenre, int page, int size) {
        return serieRepository.findByGenresIdGenre(idGenre, PageRequest.of(page, size)).map(serieMapper::serieToSerieDto);
    }
    @Override
    public Page<SerieDto> getAllSeriesByIdRealisateur(String idRealisateur, int page, int size) {
        return serieRepository.findByRealisateursIdRealisateur(idRealisateur, PageRequest.of(page, size))
                .map(serieMapper::serieToSerieDto);
    }
    @Override
    public SerieDto createSerie(SerieDto serieDto) {
        return serieMapper.serieToSerieDto(serieRepository.save(serieMapper.serieDtoToSerie(serieDto)));
    }
    @Override
    public SerieDto updateSerie(String idSerie, SerieDto serieDto) {
        var serie = serieMapper.serieDtoToSerie(getSerieById(idSerie));
        serie = serieMapper.serieDtoToSerie(serieDto);
        serie.setIdSerie(idSerie);
        return createSerie(serieMapper.serieToSerieDto(serie));
    }
    @Override
    public String deleteSerieById(String idSerie) {
        serieRepository.delete(serieMapper.serieDtoToSerie(getSerieById(idSerie)));
        return "La serie a ete supprime avec succes";
    }
}
