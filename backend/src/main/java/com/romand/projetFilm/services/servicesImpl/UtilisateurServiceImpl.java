package com.romand.projetFilm.services.servicesImpl;

import com.romand.projetFilm.dtos.UtilisateurDto;
import com.romand.projetFilm.mappers.UtilisateurMapper;
import com.romand.projetFilm.repositories.UtilisateurRepository;
import com.romand.projetFilm.services.UtilisateurService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {

    private final UtilisateurRepository utilisateurRepository;
    private final UtilisateurMapper utilisateurMapper;

    public UtilisateurServiceImpl(UtilisateurRepository utilisateurRepository, UtilisateurMapper utilisateurMapper) {
        this.utilisateurRepository = utilisateurRepository;
        this.utilisateurMapper = utilisateurMapper;
    }

    @Override
    public UtilisateurDto getUtilisateurById(String idUtilisateur) {
        return utilisateurRepository.findById(idUtilisateur).map(utilisateurMapper::utilisateurToUtilisateurDto)
                .orElseThrow(()-> new EntityNotFoundException("Aucun utilisateur avec l'id " + idUtilisateur +
                        " n'a été trouvé dans la BDD"));
    }

    @Override
    public Page<UtilisateurDto> getAllUTilisateurs(int page, int size) {
        return utilisateurRepository.findAll(PageRequest.of(page, size)).map(utilisateurMapper::utilisateurToUtilisateurDto);
    }

    @Override
    public Page<UtilisateurDto> getAllUtilisateursByIdRole(String idUTilisateur, int page, int size) {
        return utilisateurRepository.findByRolesIdRole(idUTilisateur,PageRequest.of(page, size))
                .map(utilisateurMapper::utilisateurToUtilisateurDto);
    }

    @Override
    public UtilisateurDto createUtilisateur(UtilisateurDto utilisateurDto) {
        return utilisateurMapper.utilisateurToUtilisateurDto(utilisateurRepository
                .save(utilisateurMapper.utilisateurDtoToUtilisateur(utilisateurDto)));
    }

    @Override
    public UtilisateurDto updateUtilisateur(String idUtilisateur, UtilisateurDto utilisateurDto) {
        var utilisateur = utilisateurMapper.utilisateurDtoToUtilisateur(getUtilisateurById(idUtilisateur));
        utilisateur = utilisateurMapper.utilisateurDtoToUtilisateur(utilisateurDto);
        utilisateur.setIdUtilisateur(idUtilisateur);
        return createUtilisateur(utilisateurMapper.utilisateurToUtilisateurDto(utilisateur));
    }

    @Override
    public String deleteUtilisateurById(String idUtilisateur) {
        utilisateurRepository.delete(utilisateurMapper.utilisateurDtoToUtilisateur(getUtilisateurById(idUtilisateur)));
        return "L'utilisateur a ete supprime avec succes";
    }
}
